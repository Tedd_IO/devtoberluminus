﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPointScript : MonoBehaviour {
    public GameObject thePLayer;
    public GameObject theTarget;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(thePLayer.transform.position, theTarget.transform.position, 0.33f);
	}
}
