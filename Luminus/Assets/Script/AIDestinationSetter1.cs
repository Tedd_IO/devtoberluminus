using UnityEngine;
using System.Collections;

namespace Pathfinding {
    /** Sets the destination of an AI to the position of a specified object.
	 * This component should be attached to a GameObject together with a movement script such as AIPath, RichAI or AILerp.
	 * This component will then make the AI move towards the #target set on this component.
	 *
	 * \see #Pathfinding.IAstarAI.destination
	 *
	 * \shadowimage{aidestinationsetter.png}
	 */
    [UniqueComponent(tag = "ai.destination")]
    [HelpURL("http://arongranberg.com/astar/docs/class_pathfinding_1_1_a_i_destination_setter.php")]
    public class AIDestinationSetter1 : VersionedMonoBehaviour
    {
        /** The object that the AI should move to */
        private Transform target;
        private int state = 0;
        // 0 = patrol, 1 = chase;
        private int currentPatrolIndex = 0;
        private Transform currentPatrolPoint;
        public Transform[] patrol;

        IAstarAI ai;

        void OnEnable()
        {
            ai = GetComponent<IAstarAI>();

            currentPatrolPoint = patrol[currentPatrolIndex];
            StartPatrol();

            // Update the destination right before searching for a path as well.
            // This is enough in theory, but this script will also update the destination every
            // frame as the destination is used for debugging and may be used for other things by other
            // scripts as well. So it makes sense that it is up to date every frame.
            if (ai != null) ai.onSearchPath += Update;
        }

        void OnDisable()
        {
            if (ai != null) ai.onSearchPath -= Update;
        }

        /** Updates the AI's destination every frame */
        void Update()
        {

            if (state == 0)
            {
                CheckPatrol();
                LookForLights();
            }
            else
            {

            }


            ResolveTarget();


            if (target != null && ai != null) ai.destination = target.position;
        }

        Transform FindNextChase()
        {
            // Debug.Log("INSIDE THE NULL");
            float lastDistance = 1000000;
            GameObject chosenObj = null;
            for (int a = 0; a < GlobalStuff.LightList.Count; a++)
            {


                //Debug.Log(GlobalStuff.LightList.Count);

                float dist = Vector2.Distance(transform.position, GlobalStuff.LightList[a].transform.position);
                if (dist < lastDistance)
                {
                    // Debug.Log("INSIDE THE DISTANCEIF");

                    chosenObj = GlobalStuff.LightList[a];
                    lastDistance = dist;
                }
            }

            if (chosenObj != null)
            {
                return chosenObj.transform;
            }

            return null;
        }

        void FindNextPatrol()
        {
            currentPatrolIndex++;
            if (currentPatrolIndex > patrol.Length)
            {
                currentPatrolIndex = 0;
            }
            currentPatrolPoint = patrol[currentPatrolIndex];
        }


        void ResolveTarget()
        {
            if (state == 0)
            {
                target = currentPatrolPoint;
            }
            else if (state == 1)
            {
                if (target == null)
                {
                    target = FindNextChase();
                    if(target == null)
                    {
                        StartPatrol();
                    }
                }
            }
        }

        void StartChase()
        {
            target = null;
            state = 1;
        }

        void StartPatrol()
        {
            target = currentPatrolPoint;
            state = 0;
        }

        void CheckPatrol()
        {

            if (Vector2.Distance(transform.position, currentPatrolPoint.position) < 0.1)
            {
                FindNextPatrol();
            }
        }

        void LookForLights()
        {
            if (GlobalStuff.LightList.Count != 0)
            {
                float lastDistance = 1000000;

                for (int a = 0; a < GlobalStuff.LightList.Count; a++)
                {

                    float dist = Vector2.Distance(transform.position, GlobalStuff.LightList[a].transform.position);
                    if (dist < lastDistance)
                    {
                        lastDistance = dist;
                    }
                }

                if (lastDistance < 10)
                {
                    StartChase();
                }
            }
        }
    }



    
}
