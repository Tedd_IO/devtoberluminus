﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {


    private SpriteRenderer render;
    private Rigidbody2D rb;

    //heldLight represents null if the player has no light object in it's hands, or a gameobject otherwise.
    private GameObject heldLight;
    //lightPreset is a Prefab from which light objects are spawned
    public GameObject lightPreset;
    //HandPoint is a gameobject that represents the sprites hand, for animation purposes and will likely be obsolete later on..
    public Transform HandPoint;
    private int lightsRemaining = 3;


    public float speed = 43;

    private int dirv,dirx = 0;
    private Vector2 lastdir = new Vector2(0, 0);

    //Referenced gameobjects for the target reticule
    public GameObject targetpoint,targetbase;

    //Time between pressing the E button, and the target starting to move if you just spanwed an ember.
    private float spawnlightbuffer = 0;

    private Animator anims;

    

	void Start () {
        rb = GetComponent<Rigidbody2D>();
        render = GetComponent<SpriteRenderer>();
        anims = GetComponent<Animator>();
        heldLight = transform.Find("GameLight").gameObject;
	}
	
	void Update () {
        //Standard movement ifs do the following
        // Set the current relevant direction for this frames movement
        // Update all previous direction variables
        // Do whatever must be done with the renderer
        // Shift the Target's rotation as needed
        if (Input.GetKey(KeyCode.D))
        {
            dirx = 1;
            lastdir.x = dirx;
            lastdir.y = 0;
            render.flipX = false ;
            targetbase.transform.eulerAngles = new Vector3(0,0,0);
            anims.SetInteger("Side",0);

        }
        if (Input.GetKey(KeyCode.A))
        {

            dirx = -1;
            lastdir.x = dirx;
            lastdir.y = 0;
            render.flipX = true;
            targetbase.transform.eulerAngles = new Vector3(0, 0, 180);
            anims.SetInteger("Side", 0);


        }
        if (Input.GetKey(KeyCode.W))
        {
            dirv = 1;
            lastdir.y = dirv;
            lastdir.x = 0;
            targetbase.transform.eulerAngles = new Vector3(0, 0, 90);
            anims.SetInteger("Side", 1);


        }
        if (Input.GetKey(KeyCode.S))
        {
            dirv = -1;
            lastdir.y = dirv;
            lastdir.x = 0;
            targetbase.transform.eulerAngles = new Vector3(0, 0, 270);
            anims.SetInteger("Side", 3);


        }
        if (Input.GetKey(KeyCode.E))
        {

            //Only move the target if holding after the buffer timer is 0
            if(spawnlightbuffer <= 0) { 
            targetpoint.SendMessage("Move",lastdir.x);
            }

            if (heldLight == null) 
            {
                if (lightsRemaining > 0)
                {
                    //Spawn a light if none are held
                    heldLight = Instantiate(lightPreset, HandPoint);
                    anims.SetBool("HoldingFire", true);
                    lightsRemaining -= 1;

                    //Set the buffer 
                    spawnlightbuffer = 0.2f;
                }

               

            }

        }
       
        //Actually do the moving
        rb.MovePosition(new Vector2(transform.position.x + (1f*dirx / 16f) * speed * Time.deltaTime, transform.position.y + (1f*dirv / 16f) * speed * Time.deltaTime));

        if (Input.GetKeyUp(KeyCode.E))
        {
            if(heldLight != null && spawnlightbuffer <= 0f)
            {
                Vector3[] msgpacket = new Vector3[2];
                msgpacket[0] = targetpoint.transform.position;
                msgpacket[1] = HandPoint.position;
                heldLight.transform.parent = null;
                heldLight.SendMessage("Thrown", msgpacket);
                heldLight = null;
                anims.SetBool("HoldingFire", false);
            }

            targetpoint.SendMessage("resetPos", lastdir.x);


        }

        dirv = 0;
        dirx = 0;
        
        if(spawnlightbuffer > 0)
        {
            spawnlightbuffer-= 1*Time.deltaTime;
        }

    }

}
