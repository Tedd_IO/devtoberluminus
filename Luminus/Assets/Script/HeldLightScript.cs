﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeldLightScript : MonoBehaviour  {
    private float dirx, dirv = 0;
    private float speed = 100;
    private Rigidbody2D rb;
    private float lifetime = 0;
    public Transform range1, range2, range3;
    private Vector3 initialRange1, initialRange2, initialRange3;
    private SpriteRenderer rend;
    private float throwDurationBase = 1f;
    private float throwDuration = 0;
    private float lifetimeMult = 1;
    
 

    private Vector3 target,initialthrowpos;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        initialRange1 = range1.localScale;
        initialRange2 = range2.localScale;
        initialRange3 = range3.localScale;
        rend = GetComponent<SpriteRenderer>();
        UpdateSprite();

        GlobalStuff.LightList.Add(gameObject);
       
    }

    // Update is called once per frame
    void Update () {
    
        if (!rb.isKinematic && throwDuration > 0)
        {
            // rb.MovePosition(new Vector2(transform.position.x + (1f * dirx / 16f) * speed * Time.deltaTime, transform.position.y + (1f * dirv / 16f) * speed * Time.deltaTime));
            rb.MovePosition(Vector3.Lerp(target, initialthrowpos, throwDuration));
            throwDuration -= 5 * Time.deltaTime / ((Vector3.Distance(initialthrowpos,target)/2));
        }

        lifetime += 0.03f * Time.deltaTime *lifetimeMult;

        range1.transform.localScale = Vector3.Lerp(initialRange1,new Vector3(0,0,0),lifetime);
        range2.transform.localScale = Vector3.Lerp(initialRange2, initialRange2/2, lifetime);
        range3.transform.localScale = Vector3.Lerp(initialRange3, initialRange3/1.5f, lifetime);
         lifetime = Mathf.Clamp(lifetime, 0f, 1f);

        if(lifetime == 1)
        {
            Destroy(gameObject);
            GlobalStuff.LightList.Remove(gameObject);

        }



    }

    void Thrown( Vector3[] target)
    {
        rb.isKinematic = false;
        throwDuration = throwDurationBase;
        this.target = target[0];
        initialthrowpos = target[1];
        UpdateSprite();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag.Equals("Enemy"))
        {
            lifetimeMult = 15;
        }

        dirx = 0;
        dirv = 0;
        //This needs fixing asap. V, won't work in the future
        //rb.isKinematic = true;

    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (throwDuration <= 0 && transform.parent == null)
        {
         
            if(collision.gameObject.layer == 13)
            {
                Destroy(gameObject);
                GlobalStuff.LightList.Remove(gameObject);
            }
        }
    }

    void UpdateSprite()
    {
        if(transform.parent != null)
        {
            rend.enabled = false;
        } else
        {
            rend.enabled = true;
        }
    }



  
}
