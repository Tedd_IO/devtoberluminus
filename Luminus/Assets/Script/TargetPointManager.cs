﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPointManager : MonoBehaviour {
    public GameObject player;
    public float highDistance, lowDistance;
    private Vector3 initialPos;
	// Use this for initialization
	void Start () {
        initialPos = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Move()
    {
        if (Vector3.Distance(player.transform.position, transform.position) < highDistance)
        {
            transform.Translate(3 * Time.deltaTime, 0, 0);
        }
    }

    private void resetPos()
    {
        transform.localPosition = initialPos;
    }
}
